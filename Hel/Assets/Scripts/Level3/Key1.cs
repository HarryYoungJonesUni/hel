﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key1 : MonoBehaviour {

    public InteractWithKey _hasKey;
    public Door1 _byDoor;
    public GameObject _openFirstDoor;
    public GameObject firstBarrier;

    // Update is called once per frame
    void Update()
    {
        if ((_hasKey.hasFirstKey == true) && (_byDoor.doorUnlocked == true))
        {
            _openFirstDoor.GetComponent<Renderer>().enabled = true;
            Destroy(firstBarrier);
        }
	}



}
