﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door1 : MonoBehaviour {

    public GameObject playerByDoor = null;
    public bool doorUnlocked = false;

    private void Update()
    {
        if (Input.GetButtonDown("Interact") && playerByDoor)
        {
            doorUnlocked = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D openDoor)
    {
        if (openDoor.CompareTag("Player"))
        {
            Debug.Log(openDoor.name);
            playerByDoor = openDoor.gameObject;
        }

    }

    private void OnTriggerExit2D(Collider2D openDoor)
    {
        if (openDoor.CompareTag("Player"))
        {
            if (openDoor.gameObject == playerByDoor)
            {
                playerByDoor = null;
            }

        }

    }
}
