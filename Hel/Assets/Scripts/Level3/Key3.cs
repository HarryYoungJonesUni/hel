﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key3 : MonoBehaviour {

    public InteractWithKey _hasKey;
    public Door1 _byDoor;
    public GameObject _openThirdDoor;
    public GameObject ThirdBarrier;

    // Update is called once per frame
    void Update()
    {
        if ((_hasKey.hasThirdKey == true) && (_byDoor.doorUnlocked == true))
        {
            _openThirdDoor.GetComponent<Renderer>().enabled = true;
            Destroy(ThirdBarrier);
        }
    }
}
