﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key4 : MonoBehaviour {

    public InteractWithKey _hasKey;
    public Door1 _byDoor;
    public GameObject _openFourthDoor;
    public GameObject FourthBarrier;

    // Update is called once per frame
    void Update()
    {
        if ((_hasKey.hasFourthKey == true) && (_byDoor.doorUnlocked == true))
        {
            _openFourthDoor.GetComponent<Renderer>().enabled = true;
            Destroy(FourthBarrier);
        }
    }
}
