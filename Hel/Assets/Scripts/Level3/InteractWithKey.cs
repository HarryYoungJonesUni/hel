﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractWithKey : MonoBehaviour {

    public GameObject currentKey = null;
    public GameObject firstKey;
    public GameObject secondKey;
    public GameObject thirdKey;
    public GameObject FourthKey;
    public bool hasFirstKey = false;
    public bool hasSecondKey = false;
    public bool hasThirdKey = false; 
    public bool hasFourthKey = false;

    private void Update()
    {
        if (Input.GetButtonDown("Interact") && currentKey) // if there is something in currentKey and leftmouse or E is pressed do:
        {
            if (currentKey == firstKey)
            {
                firstKey.GetComponent<Renderer>().enabled = false;
                hasFirstKey = true;
            }
            if (currentKey == secondKey)
            {
                secondKey.GetComponent<Renderer>().enabled = false;
                hasSecondKey = true;
            }
            if (currentKey == thirdKey)
            {
                thirdKey.GetComponent<Renderer>().enabled = false;
                hasThirdKey = true;
            }
            if (currentKey == FourthKey)
            {
                FourthKey.GetComponent<Renderer>().enabled = false;
                hasFourthKey = true;
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Key"))
        {
            Debug.Log(collision.name);
            currentKey = collision.gameObject;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Key"))
        {
            if (collision.gameObject == currentKey)
            {
                currentKey = null;
            }
            
        }

    }
}
