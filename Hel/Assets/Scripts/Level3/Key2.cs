﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key2 : MonoBehaviour {

    public InteractWithKey _hasKey;
    public Door1 _byDoor;
    public GameObject _openSecondDoor;
    public GameObject secondBarrier;

    // Update is called once per frame
    void Update()
    {
        if ((_hasKey.hasSecondKey == true) && (_byDoor.doorUnlocked == true))
        {
            _openSecondDoor.GetComponent<Renderer>().enabled = true;
            Destroy(secondBarrier);
        }
    }
}
