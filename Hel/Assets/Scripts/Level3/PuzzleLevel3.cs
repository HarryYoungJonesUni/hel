﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleLevel3 : MonoBehaviour {

    public bool LevelComplete = false;
    public InteractWithKey _hasFourthKey;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

         if (_hasFourthKey.hasFourthKey == true)
         {
            LevelComplete = true;
         }
}
}
