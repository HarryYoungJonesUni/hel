﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour {

    public GameObject darkness;
    public GameObject _litBrazier;
    public GameObject _openDoor;
    public GameObject _nextLevel;
	
	public void levelComplete()
    {
        Destroy(darkness);
        _litBrazier.GetComponent<Renderer>().enabled = true;
        _openDoor.GetComponent<Renderer>().enabled = true;
        _nextLevel.GetComponent<BoxCollider2D>().enabled = true;
    }
	
	
}
