﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlesCompleted : MonoBehaviour { //class to keep track of which levels have been completed for save/load purposes and for informing the brazier for a level when it can be lit

    public bool currentPuzzlestatus = false;

}
