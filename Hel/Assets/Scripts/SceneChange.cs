﻿using UnityEngine;
using System.Collections;


using UnityEngine.SceneManagement;


public class SceneChange : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
	
            // Loading level with build index
            SceneManager.LoadScene(2);	
	    }
	
	}
}
