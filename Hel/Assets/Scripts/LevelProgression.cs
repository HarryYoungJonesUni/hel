﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelProgression : MonoBehaviour {

    public bool LevelComplete;
    public bool Level1status = true;
    public bool Level2status = false;
    public bool Level3status = false;
    public PuzzleLevel2 _currentPuzzleComplete;
    public PuzzleLevel3 _level3PuzzleComplete;





    private void Start()
    {
        LevelComplete = false;
        

    }

    // Update is called once per frame
    void Update ()
    {

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level1"))
        {
            LevelComplete = true;
        }
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level2"))
        {
            if (_currentPuzzleComplete.LevelComplete == true)
            {
                LevelComplete = true;
                Level2status = true;
            }
        }
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level3"))
        {
            if (_level3PuzzleComplete.LevelComplete == true)
            {
                LevelComplete = true;
                Level3status = true;
            }
        }






    }
}
