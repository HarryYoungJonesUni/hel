﻿using UnityEngine;
using System.Collections;


using UnityEngine.SceneManagement;


public class SceneChange2 : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            // Loading level with build index
            SceneManager.LoadScene(3);
        }

    }
}

