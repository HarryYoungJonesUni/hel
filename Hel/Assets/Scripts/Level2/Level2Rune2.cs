﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2Rune2 : MonoBehaviour {

    private GameObject currentRune = null;
    public bool rune2Lit = false;
    public PuzzleLevel2 _puzzleLevel2;

    private void Update()
    {
        if (_puzzleLevel2.runeOrderCorrect >= 1)
        { 
         if (Input.GetButtonDown("Interact") && currentRune) //if left mouse is clicked and rune1 in range
          {
            rune2Lit = true;
          }
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log(other.name);
            currentRune = other.gameObject;
        }
    }

    public void OnTriggerExit2D(Collider2D other) //When leaving the radius of the rune set the variable back to null
    {
        if (other.CompareTag("Player"))
        {
            if (other.gameObject == currentRune)
            {
                currentRune = null;
            }
        }
    }
}
