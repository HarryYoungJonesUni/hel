﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleLevel2 : MonoBehaviour {

    public bool LevelComplete = false;
    public GameObject currentRune = null;
    public bool rune1Lit = false;
    public bool rune2Lit = false;
    public bool rune3Lit = false;
    public GameObject _Rune1Lit;
    public GameObject _Rune2Lit;
    public GameObject _Rune3Lit;
    public GameObject _Rune1LitSMask;
    public GameObject _Rune2LitSMask;
    public GameObject _Rune3LitSMask;
    public int runeOrderCorrect = 0;
    public Level2Rune2 _level2rune2;
    public Level2Rune3 _level2rune3;

    private void Update()
    {
        if ((rune1Lit == true) && (rune2Lit == true) && (rune3Lit == true))
        {
            LevelComplete = true;
        }



        if (Input.GetButtonDown("Interact") && currentRune)
        {
            rune1Lit = true;
            _Rune1Lit.GetComponent<Renderer>().enabled = true;
            _Rune1LitSMask.GetComponent<SpriteMask>().enabled = true;
            runeOrderCorrect += 1;
        }
        if (_level2rune2.rune2Lit)
        {
            rune2Lit = true;
            _Rune2Lit.GetComponent<Renderer>().enabled = true;
            _Rune2LitSMask.GetComponent<SpriteMask>().enabled = true;
            runeOrderCorrect += 1;
        }
        if (_level2rune3.rune3Lit)
        {
            rune3Lit = true;
            _Rune3Lit.GetComponent<Renderer>().enabled = true;
            _Rune3LitSMask.GetComponent<SpriteMask>().enabled = true;
            runeOrderCorrect += 1;
        }

    }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Debug.Log(other.name);
                currentRune = other.gameObject;
            }
        }

        public void OnTriggerExit2D(Collider2D other) //When leaving the radius of the rune set the variable back to null
        {
            if (other.CompareTag("Player"))
            {
                if (other.gameObject == currentRune)
                {
                    currentRune = null;
                }
            }
        }




    }


