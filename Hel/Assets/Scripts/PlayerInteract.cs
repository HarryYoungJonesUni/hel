﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour {

    public GameObject currentInteractObj = null;
    public EndLevel _endLevel;
    public LevelProgression _currentPuzzleComplete;
    //public PuzzleLevel2 _currentPuzzleComplete;
    public bool  puzzleComplete = false;

    private void Update()
    {
       if (_currentPuzzleComplete.LevelComplete == true)
            {
                puzzleComplete = true;
            }
         else if (_currentPuzzleComplete.LevelComplete == false)
            {
                puzzleComplete = false;
            }

        if(Input.GetButtonDown("Interact") && (currentInteractObj && puzzleComplete)) //if left mouse is clicked and there is a brazier in the currentinteractobj variable then do the following
        {
            _endLevel.levelComplete();
        }
    }

    public void OnTriggerEnter2D(Collider2D other) //When within the Braziers 2D trigger collider set currentInteractObj gameobject as that brazier 
    {
        if (other.CompareTag("Brazier"))
        {
            Debug.Log(other.name);
            currentInteractObj = other.gameObject;
        }
    }

    public void OnTriggerExit2D(Collider2D other) //When leaving the radius of the brazier set the variable back to null
    {
        if (other.CompareTag("Brazier"))
        {
            if (other.gameObject == currentInteractObj)
            {
                currentInteractObj = null;
            }
        }
    }
}
